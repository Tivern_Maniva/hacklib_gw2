#include "main.h"
#include "hacklib/PatternScanner.h"
#include "hacklib/Logging.h"

#define VERIFY_CB [](uintptr_t o, uintptr_t val)->bool

namespace GW2LIB {

    /*
    * DataScan():
    *  pattern = the pattern to search for, passed to hl::FindPattern() or hl::PatternScanner::find()
    *  shiftCb = custom func to call for shifting the offset, "shift" and "size" are ignored
    *  shift = the relative offset from the pattern to the target value
    *  size = number of bytes to extract from the target offset
    *  isString = which scan type to use hl::FindPattern() or hl::PatternScanner::find()
    *  verify = callback to verify if offset is correct
    */
    uintptr_t DataScan(const char *pattern, uintptr_t(*shiftCb)(uintptr_t offset), int32_t shift, uint8_t size, bool isString, bool(*verify)(uintptr_t offset, uintptr_t value)) {
        uintptr_t val = 0;
        uintptr_t offset = 0;

        try {
            if (isString) {
                static hl::PatternScanner scanner;
                auto results = scanner.find({ pattern });
                offset = results[0];
            } else {
                offset = hl::FindPattern(pattern);
            }
        } catch (...) {
            offset = 0;
        }

        if (!offset) {
            HL_LOG_ERR("[GW2LIB::DataScan] Could not find pattern: \"%s\"\n", pattern);
            throw STATUS_ACCESS_VIOLATION;
        }

        if (shiftCb) { // PointerScan
            try {
                val = shiftCb(offset);
            } catch (...) {
                HL_LOG_ERR("[GW2LIB::DataScan] Exception occured in callback for pattern: \"%s\"\n", pattern);
                throw STATUS_ACCESS_VIOLATION;
            }
        } else { // OffsetScan
            offset += shift;

            while (size--) {
                val |= (*(uint8_t*)(offset + size)) << (size * 8);
            }
        }

        if (verify && !verify(offset, val)) {
            HL_LOG_ERR("[GW2LIB::DataScan] Verifying pattern failed (offset: 0x%p, value: 0x%X, pattern: \"%s\")\n", offset, val, pattern);
            throw STATUS_ACCESS_VIOLATION;
        }

        return val;
    }

    uintptr_t OffsetScan(const char *pattern, int32_t shift, uint8_t size, bool isString, bool(*verify)(uintptr_t offset, uintptr_t value)) {
        return DataScan(pattern, nullptr, shift, size, isString, verify);
    }

    uintptr_t PointerScan(const char *pattern, uintptr_t(*cb)(uintptr_t offset), bool isString, bool(*verify)(uintptr_t offset, uintptr_t value)) {
        return DataScan(pattern, cb, 0, 0, isString, verify);
    }
};

bool Gw2HackMain::ScanForOffsets() {
    using namespace GW2LIB;
    GW2LIB::Mems &m = m_pubmems;

    try {
#ifdef ARCH_64BIT

#else

        m.contextChar = OffsetScan("!IsPlayer() || GetPlayer()", 0x10, 1, true, VERIFY_CB{ return val >= 0x30 && val <= 0x60; });
        m.avagVtGetAgent = OffsetScan("wmAgent->GetAgent() != agent", -0x10, 1, true, VERIFY_CB{ return val >= 0x24 && val <= 0x60; });
        m.wmAgVtGetCodedName = OffsetScan("85 C0 74 ?? 8B 10 8B C8 FF 52 ?? 85 C0 74 ?? 8B D0 8B CE E8 ?? ?? ?? ?? 8B F0 8B 4B 54 E8", 0xa, 1, false, VERIFY_CB{ return val >= 0x30 && val <= 0x60; });
        m.agentVtGetCategory = OffsetScan("agent->GetCategory() == AGENT_CATEGORY_CHAR", -0xd, 1, true, VERIFY_CB{ return val > 0 && val <= 0x30; });
        m.agentVtGetId = OffsetScan("targetAgent && targetAgent->GetAgentId()", -0x10, 1, true, VERIFY_CB{ return val >= 0x30 && val <= 0x80; });
        m.agentVtGetType = OffsetScan("m_outOfRangeActivationTargetAgent->GetType() == AGENT_TYPE_GADGET_ATTACK_TARGET", -0x14, 4, true, VERIFY_CB{ return val >= 0x90 && val <= 0xc0; });

        m.charID = OffsetScan("8B 42 ?? 89 45 E4 89 4A ?? 8B 42 ?? 8B 4D E8 89 45 E8 89 4A ?? 8B 42 ?? 8B 4D EC 89 45 EC 89 4A ?? 8B 4D F0 8B 42 ?? 89 45 F0 89 4A ?? 8B 47 ?? 8B 4B", 0x32, 1, false, VERIFY_CB{ return val >= 0x30 && val <= 0x60; });
        m.charVtGetAgentId = OffsetScan("m_agent && (m_agent->GetAgentId() == character->GetAgentId() || m_masterCharacter == character)", -0x19, 4, true, VERIFY_CB{ return val > 0 && val < 0x200; });
        m.charVtGetPlayer = OffsetScan("m_ownerCharacter->GetPlayer() == CharClientContext()->GetControlledPlayer()", -0x1f, 4, true, VERIFY_CB{ return val > 0 && val < 0x400; });

#endif
    } catch (...) {
        return false;
    }

    return true;
}

bool Gw2HackMain::ScanForPointers() {
    using namespace GW2LIB;
    GamePointers &m = m_mems;

    try {
#ifdef ARCH_64BIT

#else
        m.avctxAgentArray = PointerScan("52 FF 50 ?? C7 86 ?? ?? ?? ?? 00 00 00 00 56 E8 ?? ?? ?? ?? 8B C8 E8", [](uintptr_t o)->uintptr_t {
            return *(uintptr_t*)(hl::FollowRelativeAddress(o + 0x10) + 1);
        }, false, VERIFY_CB{ return true; }) + 4;


#endif
    } catch (...) {
        return false;
    }

    return true;
}


