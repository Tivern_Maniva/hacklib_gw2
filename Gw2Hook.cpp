
#include "main.h"

size_t __fastcall hkCanBeSel(uintptr_t, uintptr_t, uintptr_t);

void hkProcessText(hl::CpuContext*);
void hkDmgLog(hl::CpuContext*);
void hkCombatLog(hl::CpuContext*);
void hkAllocator(hl::CpuContext*);
void hkLogger(hl::CpuContext*);
void hkLogger2(hl::CpuContext*);
void hkFrameText(hl::CpuContext*);
void hkMsgConn(hl::CpuContext*);
void hkScreenshot(hl::CpuContext*);
void hkWndProc(hl::CpuContext*);
void hkTextDec(hl::CpuContext*);

// "ArenaNet_Dx_Window_Class" = main window class
bool Gw2GameHook::init_hooks() {
#ifdef ARCH_64BIT
    uintptr_t pLogger  = hl::FindPattern("48 89 5C 24 08 57 48 83 EC 20 4D 8B 09 48 8B DA 48 8B F9 E8");
    uintptr_t pLogger2 = hl::FindPattern("5F C3 CC CC CC CC CC CC CC 48 89 5C 24 08 57 48 83 EC 20 4D 8B 09 48 8B DA 48 8B F9 E8");
    uintptr_t pScreenshot = hl::FindPattern("48 89 74 24 18 57 48 83 EC 30 8B F1 8B F9 83 E6 02 74");
    uintptr_t pWndProc = hl::FindPattern("40 53 55 56 41 55 41 56 41 57 48 83 EC 68 48 8B 05 ?? ?? ?? ?? 48 33 C4 48 89 44 24 48 89 54 24 20 48 89 7C 24 60 8B FA");
#else
    uintptr_t pLogger = hl::FindPattern("55 8B EC 8B 45 0C 53 56 57 FF 30 8B FA 8B D9 FF 75 08 57 53 E8");
    uintptr_t pLogger2 = hl::FindPattern("5D C2 08 00 CC CC CC CC CC CC CC CC CC CC CC 55 8B EC 8B 45 0C 53 56 57 FF 30 8B FA 8B D9 FF 75 08 57 53 E8");
    uintptr_t pFrameTxt = hl::FindPattern("FF 50 04 5F 5E 5B 8B E5 5D C2 24 00 CC CC CC 52 51");
    uintptr_t pScreenshot = hl::FindPattern("55 8B EC 51 53 56 8B D9 8B F3 83 E6 02 89 75 FC 74");
    uintptr_t pWndProc = hl::FindPattern("55 8B EC 83 EC 30 A1 ?? ?? ?? ?? 33 C5 89 45 FC 8B 45 08 8B 4D 14 53 56 57 6A EB 50 89 45 E0 89 4D E8 FF 15");
    uintptr_t pTextDec = hl::FindPattern("8B 0A 89 4D E8 8B 52 04 89 55 EC EB ?? 8B 55 EC 8B 4D E8 52 51 FF 75 0C 8B CF FF 75 08 53 E8");
#endif

    hl::PatternScanner scanner;
    auto results = scanner.find({
        "codedProcessedText",
        "targetAgent",
        "logType < UI_COMBAT_LOG_TYPES",
        "bytes < MAX_ALLOC",
        "mc->recvMsgPacked->defArray[0].defSize"
    });

    uintptr_t pProcessText = NULL;
    uintptr_t pDmgLog = NULL;
    uintptr_t pCombatLog = NULL;
    uintptr_t pAllocator = NULL;
    uintptr_t pMsgConn = NULL;

#ifdef ARCH_64BIT
    pProcessText = (results[0] - 0x49);
    pDmgLog = (results[1] - 0x2a);
    pCombatLog = (results[2] - 0x20);
    pAllocator = (results[3] - 0x4d);
    pLogger = (pLogger + 0x4c);
    pLogger2 = (pLogger2 + 0x55);
    pWndProc = (pWndProc + 0x18);

    m_hkProcessText = m_hooker.hookDetour(pProcessText, 17, hkProcessText);
    m_hkDmgLog = m_hooker.hookDetour(pDmgLog, 15, hkDmgLog);
    m_hkCombatLog = m_hooker.hookDetour(pCombatLog, 16, hkCombatLog);
    m_hkAllocator = m_hooker.hookDetour(pAllocator, 14, hkAllocator);
    m_hkLogger = m_hooker.hookDetour(pLogger, 14, hkLogger);
    m_hkLogger2 = m_hooker.hookDetour(pLogger2, 14, hkLogger2);
    m_hkScreenshot = m_hooker.hookDetour(pScreenshot, 14, hkScreenshot);
    m_hkWndProc = m_hooker.hookDetour(pWndProc, 14, hkWndProc);
#else
    pProcessText = (results[0] - 0x2d);
    pDmgLog = (results[1] - 0x10);
    pCombatLog = (results[2] - 0x14);
    pAllocator = (results[3] - 0x21);
    pMsgConn = (results[4] - 0x17);
    pLogger = (pLogger + 0x19);
    pLogger2 = (pLogger2 + 0x28);
    pFrameTxt = (pFrameTxt + 0xf);
    pScreenshot = (pScreenshot + 0x3);
    pWndProc = (pWndProc + 0xd);
    pTextDec = (pTextDec + 0x23);

    m_hkProcessText = m_hooker.hookDetour(pProcessText, 6, hkProcessText);
    m_hkDmgLog = m_hooker.hookDetour(pDmgLog, 6, hkDmgLog);
    m_hkCombatLog = m_hooker.hookDetour(pCombatLog, 7, hkCombatLog);
    m_hkAllocator = m_hooker.hookDetour(pAllocator, 5, hkAllocator);
    m_hkLogger = m_hooker.hookDetour(pLogger, 5, hkLogger);
    m_hkLogger2 = m_hooker.hookDetour(pLogger2, 5, hkLogger2);
    m_hkFrTxt = m_hooker.hookDetour(pFrameTxt, 7, hkFrameText);
    m_hkMsgConn = m_hooker.hookDetour(pMsgConn, 6, hkMsgConn);
    m_hkScreenshot = m_hooker.hookDetour(pScreenshot, 7, hkScreenshot);
    m_hkWndProc = m_hooker.hookDetour(pWndProc, 6, hkWndProc);
    //m_hkTextDec = m_hooker.hookDetour(pTextDec, 7, hkTextDec);
#endif

    auto mems = GetMain()->GetGamePointers();

    m_hkCanBeSel = m_hooker.hookVT((uintptr_t)mems->pAgentSelectionCtx, 1, (uintptr_t)hkCanBeSel);
    if (!m_hkCanBeSel) {
        HL_LOG_ERR("[Hook::Init] Hooking m_hkCanBeSel failed\n");
        return false;
    }

    if (!m_hkProcessText) {
        HL_LOG_ERR("[Hook::Init] Hooking chat log failed\n");
        return false;
    }

    if (!m_hkDmgLog) {
        HL_LOG_ERR("[Hook::Init] Hooking damage log failed\n");
        return false;
    }

    if (!m_hkCombatLog) {
        HL_LOG_ERR("[Hook::Init] Hooking combat log failed\n");
        return false;
    }

    if (!m_hkLogger) {
        HL_LOG_ERR("[Hook::Init] Hooking game logger failed\n");
        return false;
    }

    if (!m_hkLogger2) {
        HL_LOG_ERR("[Hook::Init] Hooking game logger 2 failed\n");
        return false;
    }

    if (!m_hkFrTxt) {
        HL_LOG_ERR("[Hook::Init] Hooking frame text proc failed\n");
        return false;
    }

    if (!m_hkScreenshot) {
        HL_LOG_ERR("[Hook::Init] Hooking screenshot proc failed\n");
        return false;
    }

    if (!m_hkWndProc) {
        HL_LOG_ERR("[Hook::Init] Hooking window proc failed\n");
        return false;
    }

    return true;
}

void Gw2GameHook::cleanup() {
    if (m_hkCanBeSel) m_hooker.unhook(m_hkCanBeSel);
    if (m_hkProcessText) m_hooker.unhook(m_hkProcessText);
    if (m_hkDmgLog) m_hooker.unhook(m_hkDmgLog);
    if (m_hkCombatLog) m_hooker.unhook(m_hkCombatLog);
    if (m_hkAllocator) m_hooker.unhook(m_hkAllocator);
    if (m_hkLogger) m_hooker.unhook(m_hkLogger);
    if (m_hkLogger2) m_hooker.unhook(m_hkLogger2);
    if (m_hkFrTxt) m_hooker.unhook(m_hkFrTxt);
    if (m_hkScreenshot) m_hooker.unhook(m_hkScreenshot);
    if (m_hkWndProc) m_hooker.unhook(m_hkWndProc);
    if (m_hkTextDec) m_hooker.unhook(m_hkTextDec);
}


// this hook will intercept calls to "AgentCanBeSelection()" and run a user provided function.
// setting "call_orig" to true inside your callback will call the original game function and
// ignore any return value provided by the callback
size_t __fastcall hkCanBeSel(uintptr_t pInst, uintptr_t ag64, uintptr_t ag32) {
#ifdef ARCH_64BIT
    uintptr_t agptr = ag64;
#else
    uintptr_t agptr = ag32;
#endif

    Gw2GameHook *hk = get_hook();
    Gw2Hooks* list = get_hook_list();
    auto pCore = GetMain();
    static auto orgFunc = ((bool(__thiscall*)(uintptr_t, uintptr_t))hk->m_hkCanBeSel->getLocation());

    bool call_orig = false;
    bool ret = false;

    try {
        if (list->AgCanBeSel) {
            GW2LIB::Agent ag(agptr);
            ret = list->AgCanBeSel(call_orig, ag);
        } else {
            call_orig = true;
        }

        if (call_orig) return orgFunc(pInst, agptr);
        return ret;
    } catch (int e) {
        HL_LOG_ERR("[hkCanBeSel Exception]: %i\n", e);
    }

    return false;
}

struct mc_msg_def {
    uintptr_t unk1;
    uintptr_t unk2;
    uintptr_t type;
    uintptr_t unk3;
    uintptr_t size;
};

struct mc_msg {
    uintptr_t arg1;
    mc_msg_def *def; // ptr to msg def
    uintptr_t arg3;
    uintptr_t func;  // msg handler
};

void hkMsgConn(hl::CpuContext *ctx) {
#ifdef ARCH_64BIT

#else
    // mc->recvMsgPacked->defArray[0].defSize
    // ctx->ESI = mc struct
    mc_msg *msg = *(mc_msg**)(ctx->ESI + 0x28);

    // recvMsgPacked
    uintptr_t arg1  = msg->arg1;
    mc_msg_def *def = msg->def;
    uintptr_t arg3  = msg->arg3;
    uintptr_t func  = msg->func;

    // msg def
    uintptr_t unk1 = def->unk1;
    uintptr_t type = def->type;
    uintptr_t size = def->size;

    //HL_LOG_DBG("msg: %p, arg1: %p, def: %p, func: %p, type: %p, size: %p\n", msg, arg1, def, func, type, size);
#endif
}

void hkTextDec(hl::CpuContext *ctx) {
#ifdef ARCH_64BIT
    wchar_t *wtxt = L"";
#else
    wchar_t *wtxt = (wchar_t*)(ctx->EAX);
#endif

    std::string txt = get_hook()->converter.to_bytes(wtxt);

    if (txt.size() &&
        txt != "%str1%" &&
        txt != "F%num1%" &&
        txt != "A") {
        HL_LOG_DBG("%s\n", txt.c_str());
    }
}

void hkWndProc(hl::CpuContext *ctx) {
#ifdef ARCH_64BIT
    HWND &hWnd = (HWND&)(ctx->RCX);
    UINT &msg = (UINT&)(ctx->RDX);
    WPARAM &wParam = (WPARAM&)(ctx->R8);
    LPARAM &lParam = (LPARAM&)(ctx->R9);
#else
    HWND &hWnd = *(HWND*)(ctx->EBP + 0x8);
    UINT &msg = *(UINT*)(ctx->EBP + 0xc);
    WPARAM &wParam = *(WPARAM*)(ctx->EBP + 0x10);
    LPARAM &lParam = *(LPARAM*)(ctx->EBP + 0x14);
#endif

    Gw2Hooks* list = get_hook_list();
    bool call_orig = true;
    if (list->WndProcHook) call_orig = list->WndProcHook(hWnd, msg, wParam, lParam);
    if (!call_orig) msg = WM_NULL;
}

void hkScreenshot(hl::CpuContext *ctx) {
    using namespace GW2LIB::GW2;
#ifdef ARCH_64BIT
    auto &reg = ctx->RCX;
#else
    auto &reg = ctx->ECX;
#endif

    auto mode = (ScreenshotMode)reg;

    Gw2Hooks* list = get_hook_list();
    if (list->ScreenshotHook) {
        mode = list->ScreenshotHook(mode);
    }

    if(mode >= 0 && mode < SCREENSHOT_END)
        reg = mode;
}


void hkFrameText(hl::CpuContext *ctx) {
#ifdef ARCH_64BIT
    uintptr_t p1 = (uintptr_t)ctx->RCX;
    wchar_t *wtxt = (wchar_t*)ctx->RDX;
#else
    uintptr_t p1 = (uintptr_t)ctx->ECX;
    wchar_t *wtxt = (wchar_t*)ctx->EDX;
#endif

    Gw2GameHook *hk = get_hook();
    std::string test = hk->converter.to_bytes(wtxt);
    //if(test != "A+F1" && test != "A+F2" && test.size()) HL_LOG_DBG("ECX: %p - EDX: %p - %s\n", p1, wtxt, test.c_str());
}

void hkProcessText(hl::CpuContext *ctx)
{
#ifdef ARCH_64BIT
    wchar_t *wtxt = *(wchar_t**)(ctx->RDX + 0x8);
#else
    wchar_t *wtxt = (wchar_t*)ctx->ECX;
#endif

    Gw2Hooks* list = get_hook_list();
    if (list->ChatHook) list->ChatHook(wtxt);
}


void hkDmgLog(hl::CpuContext *ctx)
{
#ifdef ARCH_64BIT
    int hit = (int)ctx->R9;
    uintptr_t *pSrc = (uintptr_t*)(ctx->R8);
    uintptr_t *pTgt = (uintptr_t*)(ctx->RDX);
    hl::ForeignClass skillDef = **(uintptr_t***)(ctx->RSP + 0x28);
#else
    int hit = *(int*)(ctx->EBP + 0x10);
    uintptr_t *pSrc = *(uintptr_t**)(ctx->EBP + 0xC);
    uintptr_t *pTgt = *(uintptr_t**)(ctx->EBP + 0x8);
    hl::ForeignClass skillDef = *(uintptr_t**)(ctx->EBP + 0x14);
#endif

    const GW2LIB::Mems *offsets = GetMain()->GetGameOffsets();

    GW2LIB::GW2::EffectType ef = skillDef.get<GW2LIB::GW2::EffectType>(offsets->skillDefEffect);
    GW2LIB::Agent agSrc(pSrc);
    GW2LIB::Agent agTgt(pTgt);

    Gw2Hooks* list = get_hook_list();
    if (list->DmgLogHook) list->DmgLogHook(agSrc, agTgt, hit, ef);
}

void hkCombatLog(hl::CpuContext *ctx)
{
#ifdef ARCH_64BIT
    GW2LIB::GW2::CombatLogType type = (GW2LIB::GW2::CombatLogType)(ctx->R8);
    int hit = *(int*)(ctx->RSP + 0x4c);
    uintptr_t *pSrc = *(uintptr_t**)(ctx->RBX + 0x40);
    uintptr_t *pTgt = *(uintptr_t**)(ctx->RBX + 0x58);
    hl::ForeignClass skillDef = *(uintptr_t**)(ctx->RBX + 0x48);
#else
    GW2LIB::GW2::CombatLogType type = *(GW2LIB::GW2::CombatLogType*)(ctx->EBP + 0xC);
    int hit = *(int*)(ctx->EBP + 0x18);
    uintptr_t *pSrc = *(uintptr_t**)(*(uintptr_t*)(ctx->EBP + 0x14) + 0x28);
    uintptr_t *pTgt = *(uintptr_t**)(*(uintptr_t*)(ctx->EBP + 0x14) + 0x34);
    hl::ForeignClass skillDef = *(uintptr_t**)(*(uintptr_t*)(ctx->EBP + 0x14) + 0x2c);
#endif

    const GW2LIB::Mems *offsets = GetMain()->GetGameOffsets();

    GW2LIB::GW2::EffectType ef = skillDef.get<GW2LIB::GW2::EffectType>(offsets->skillDefEffect);
    GW2LIB::Agent agTgt(pTgt);
    GW2LIB::Agent agSrc(pSrc);

    Gw2Hooks* list = get_hook_list();
    if (list->CombatLogHook) list->CombatLogHook(agSrc, agTgt, hit, type, ef);
}

void hkAllocator(hl::CpuContext *ctx) {
#ifdef ARCH_64BIT
    int type = (int)(ctx->RCX);
    size_t size = ctx->RDX;
    int cat = *(int*)(ctx->RSP + 0x38);
    int line = *(int*)(ctx->RSP + 0x30);
    char* file = *(char**)(ctx->RSP + 0x28);
#else
    int type = ctx->ECX;
    size_t size = ctx->EDX;
    int cat = *(int*)(ctx->EBP + 0x18);
    int line = *(int*)(ctx->EBP + 0x14);
    char* file = *(char**)(ctx->EBP + 0x10);
#endif

    Gw2Hooks* list = get_hook_list();
    if (list->AllocatorHook) list->AllocatorHook(type, size, cat, line, file);
}

void hkLogger(hl::CpuContext *ctx) {
#ifdef ARCH_64BIT
    char* txt = (char*)(ctx->RDI);
#else
    char* txt = (char*)(ctx->EBX);
#endif

    Gw2Hooks* list = get_hook_list();
    if (list->LoggerHook) list->LoggerHook(txt);
}

void hkLogger2(hl::CpuContext *ctx) {
#ifdef ARCH_64BIT
    wchar_t* wtxt = (wchar_t*)(ctx->RDI);
#else
    wchar_t* wtxt = (wchar_t*)(ctx->EBX);
#endif

    std::string txt = get_hook()->converter.to_bytes(wtxt);

    Gw2Hooks* list = get_hook_list();
    if (list->LoggerHook) list->LoggerHook((char*)txt.c_str());
}


