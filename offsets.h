#ifndef OFFSETS_H
#define OFFSETS_H

namespace GW2LIB {
    struct Mems {
#ifdef ARCH_64BIT
        uintptr_t contextChar = 0x90;
        uintptr_t avctxAgentArray = 0x70;
        uintptr_t avagVtGetAgent = 0x60;
        uintptr_t agentVtGetCategory = 0x20;
        uintptr_t agentVtGetId = 0xc0;
        uintptr_t agentVtGetType = 0x140;
        uintptr_t agentVtGetPos = 0x120;
        uintptr_t agentTransform = 0x28;
        uintptr_t agtransX = 0x30;
        uintptr_t agtransY = 0x34;
        uintptr_t agtransZ = 0x38;
        uintptr_t gd_agtransVtGetRot = 0x130;
        uintptr_t agtransVtGetMoveRot = 0x158;
        uintptr_t agtransVtGetRot = 0x160;
        uintptr_t agtransToken = 0xf0;
        uintptr_t agtransSeq = 0xf8;
        uintptr_t npc_agtransSpeed = 0x148;
        uintptr_t agtransRealSpeed = 0x204;
        uintptr_t agtransSpeed = 0x208;
        uintptr_t agtransMaxSpeed = 0x20c;

        uintptr_t charctxCharArray = 0x60;
        uintptr_t charctxPlayerArray = 0x80;
        uintptr_t charctxControlled = 0x98;

        uintptr_t charVtGetAgent = 0x0;
        uintptr_t charVtGetAgentId = 0x188;
        uintptr_t charVtGetPlayer = 0x250;
        uintptr_t charVtAlive = 0x8;
        uintptr_t charVtControlled = 0x2e8;
        uintptr_t charVtDowned = 0x300;
        uintptr_t charVtInWater = 0x370;
        uintptr_t charVtMonster = 0x388;
        uintptr_t charVtClone = 0x388;
        uintptr_t charVtPlayer = 0x3e8;
        uintptr_t charVtRangerPet = 0x3e0;
        uintptr_t charAttitude = 0xa8;
        uintptr_t charBreakbar = 0xb0;
        uintptr_t charCoreStats = 0x2a0;
        uintptr_t charEndurance = 0x2e8;
        uintptr_t charHealth = 0x2f8;
        uintptr_t charInventory = 0x300;
        uintptr_t charVtGetCmbtnt = 0x108;
        uintptr_t charGliderPercent = 0x138;
        uintptr_t charProfession = 0x3f8;
        uintptr_t charName = 0x190;
        uintptr_t charFlags = 0x130;
        uintptr_t charID = 0x90;
        uintptr_t charSubClass = 0x8;

        uintptr_t cmbtntBuffBar = 0x88;
        uintptr_t buffbarBuffArr = 0x10;

        uintptr_t buffEfType = 0x8;
        uintptr_t buffSkillDef = 0x10;
        uintptr_t buffBuffId = 0x18;
        uintptr_t buffSrcAg = 0x28;
        uintptr_t buffDuration = 0x40;
        uintptr_t buffActive = 0x4c;

        uintptr_t skillDefEffect = 0x28;
        uintptr_t skillDefInfo = 0x60;
        uintptr_t skillStackType = 0xc;

        uintptr_t playerName = 0x68;
        uintptr_t playerVtGetWallet = 0x1a0;
        uintptr_t playerVtGetTrainMgr = 0x2d0;
        uintptr_t playerVtGetAchMgr = 0x130;
        uintptr_t playerVtGetSpecMgr = 0x2a0;
        uintptr_t playerChar = 0x20;

        uintptr_t walletVtGetCurrency = 0x0;
        uintptr_t trmgrVtGetMLvl = 0x18;
        uintptr_t achMgrVtGetAP = 0x18;
        uintptr_t specMgrSpecsArr = 0x40;
        uintptr_t specMgrTraitsArr = 0x60;
        uintptr_t specType = 0x28;
        uintptr_t traitType = 0x28;

        uintptr_t statsRace = 0x33;
        uintptr_t statsGender = 0x35;
        uintptr_t statsStats = 0xac;
        uintptr_t statsLevel = 0x1ec;
        uintptr_t statsScaledLevel = 0x21c;
        uintptr_t statsProfession = 0x264;
        uintptr_t endCurrent = 0x8;
        uintptr_t endMax = 0xc;
        uintptr_t healthCurrent = 0xc;
        uintptr_t healthMax = 0x10;

        uintptr_t invSupply = 0x3cc;
        uintptr_t invEquipArr = 0x140;

        uintptr_t equipItemDef = 0x40;
        uintptr_t equipItemInv = 0x58;
        uintptr_t equipItemVtGetUpgrades = 0x208;
        uintptr_t equipItemUpgradesArr = 0x28;
        uintptr_t equipItemVtGetSkin = 0x70;

        uintptr_t itemDefId = 0x28;
        uintptr_t itemDefRarity = 0x60;

        uintptr_t asctxAuto = 0x50;
        uintptr_t asctxHover = 0xf8;
        uintptr_t asctxLocked = 0x230;
        uintptr_t asctxStoW = 0x298;
        uintptr_t asctxCtxMode = 0x70;
        uintptr_t asctxVtAgCanSel = 0x8;
        uintptr_t wvctxVtGetMetrics = 0x78;
        uintptr_t wvctxStatus = 0x58;

        uintptr_t camCurrZoom = 0x48;
        uintptr_t camMinZoom = 0x38;
        uintptr_t camMaxZoom = 0x3c;

        uintptr_t breakbarState = 0x40;
        uintptr_t breakbarPercent = 0x44;

        uintptr_t compWidth = 0x40;
        uintptr_t compHeight = 0x44;
        uintptr_t compZoom = 0x48;
        uintptr_t compFlags = 0x28;
        uintptr_t compMaxWidth = 0x18;
        uintptr_t compMaxHeight = 0x1c;

        uintptr_t uiIntSize = 0x54;
        uintptr_t uiFlags1 = 0x1b0;
        uintptr_t uiFlags2 = 0x1b4;

        uintptr_t contextGadget = 0x130;
        uintptr_t ctxgdVtGetGadget = 0x10;
        uintptr_t ctxgdVtGetAtkTgt = 0x20;

        uintptr_t atkTgtGd = 0x58;
        uintptr_t gdHealth = 0x208;
        uintptr_t gdWvwTeamId = 0x470;
        uintptr_t gdVtGetType = 0xc0;
        uintptr_t gdVtGetRNode = 0x110;

        uintptr_t rnodeType = 0xc;
        uintptr_t rnodeFlags = 0x10;

        uintptr_t profStance = 0x40;
        uintptr_t profEnergy = 0x50;
        uintptr_t profEnergyMax = 0x54;
#else
        /*
        If you update gw2lib and the patterns are still working it can be useful to know
        the current game object pointers for debugging. The following example shows how to
        get them. If the patterns break, see Gw2HackMain::init the strings are unlikly to
        change but maybe the offsets if code generation is changed.

        #include "main.h"

        void dummy()
        {
            auto ptrs = GetMain()->GetGamePointers();
            void *pCtx = ptrs->pCtx;
        }

        It may also be helpful if you attach a debugger and break on thrown Win32 exeptions, so
        you know the member or function offset that causes the access violation.
        */

        // CContext
        // CharClient::CContext* m_charContext;
        uintptr_t contextChar = 0x48; // os
        /*
        This is the main game state object from which you can get to almost anything else. It can
        only be safely accessed from within the game thread.
        A pointer to this object can be found in the thread-local storage of the game thread. Windows
        stores these in FS:[0x2c].
        The current location is: (unlikely to change)
        CContext ***localStorage = FS:[0x2c];
        CContext *ctx = localStorage[0][1];

        It consists of only pointers to other sub contexts and nulled out space.

        The char context offset can be found by looking at the objects before and after the offset
        where it was before and compare to the CharClient::CContext description.
        "!IsPlayer() || GetPlayer()" + 0xE = 32-bit mov opcode (mov ebx,[eax+??])
        */

        // AgentView::CContext
        // ANet::Array<Agent::CAvAgent*> m_agentArray;
        uintptr_t avctxAgentArray = 0x38; // os
        /*
        This is the context that deals with agents, the base of all entities in the game. It is optimized
        out of the main context, but there are static references to it.

        The agent array offset is found by looking for arrays in this object.

        An array has a base pointer that points to the raw data array and two following integers that
        describe the capacity and current size of the array. This layout is really easy to recognize.
        "52 FF 50 ?? C7 86 ?? ?? ?? ?? 00 00 00 00 56 E8 ?? ?? ?? ?? 8B C8 E8" + 0x10 = 32-bit call to Agent Array fn
        */

        // AgentView::CAvAgent
        // Agent::CAgentBase* GetAgent();
        uintptr_t avagVtGetAgent = 0x30; // os
        /*
        We are not interested in this object, but it is used to get from CAvAgent to CAgentBase which
        holds useful information.

        The offset of the virtual function can be found with the following assert string:
        "wmAgent->GetAgent() != agent"
        */

        uintptr_t wmAgVtGetCodedName = 0x38; // os

        // Agent::CAgentBase
        // AgentCategory GetCategory();
        uintptr_t agentVtGetCategory = 0x10; // os
        // int GetAgentId();
        uintptr_t agentVtGetId = 0x60; // os
        // AgentType GetType();
        uintptr_t agentVtGetType = 0xa0; // os
        // void GetPos(D3DXVECTOR3* pPos);
        uintptr_t agentVtGetPos = 0x8c;
        // Agent::CAgentTransform* m_transform;
        uintptr_t agentTransform = 0x1c;

        /*
        Contains agent data that is important to this lib.

        Assert strings for virtual functions in order:
        "agent->GetCategory() == AGENT_CATEGORY_CHAR"
        "targetAgent && targetAgent->GetAgentId()"
        "m_outOfRangeActivationTargetAgent->GetType() == AGENT_TYPE_GADGET_ATTACK_TARGET"
        GetPos I don't remember, but should be easy to trial and error.
        "55 8B EC 8B 49 1C FF 75 08 8B 11 FF 52 3C 8B 45 08 5D C2 04 00" = 32-bit agentVtGetPos func def

        The agentTransform member is very easy to recognize, because many numbers in it
        move when your character moves (when looking at own agent data). See CAgentTransform.
        */

        // Agent::CAgentTransform
        // float x;
        uintptr_t agtransX = 0x20;
        // float y;
        uintptr_t agtransY = 0x24;
        // float z;
        uintptr_t agtransZ = 0x28;

        uintptr_t gd_agtransVtGetRot = 0x98;
        uintptr_t agtransVtGetMoveRot = 0xac;
        uintptr_t agtransVtGetRot = 0xb0;

        uintptr_t agtransToken = 0xa8;
        uintptr_t agtransSeq = 0xb0;
        uintptr_t npc_agtransSpeed = 0x100;
        uintptr_t agtransRealSpeed = 0x1b4;
        uintptr_t agtransSpeed = 0x1b8;
        uintptr_t agtransMaxSpeed = 0x1bc;
        //uintptr_t transVtGetSeq = 0x88; // void GetSeq(_out_ UINT64 &buf);
        /*
        Holds metric information about an agent.

        The offsets are easy to figure out if you look at data of your own agent and
        move or rotate.
        */

        //uintptr_t wmagTransScale = 0x9f4;

        // CharClient::CContext
        // ANet::Array<CharClient::CCharacter*> m_charArray;
        uintptr_t charctxCharArray = 0x34;
        // ANet::Array<CharClient::CPlayer*> m_playerArray;
        uintptr_t charctxPlayerArray = 0x48;
        // CharClient::CCharacter* m_controlledCharacter;
        uintptr_t charctxControlled = 0x58;
        /*
        Context that contains data about CCharacters.

        Easy to recognize by the arrays. The order of them is unlikely to change.
        */

        // CharClient::CCharacter
        uintptr_t charVtGetHealth = 0x2c;
        // Agent::CAgentBase* GetAgent();
        uintptr_t charVtGetAgent = 0x0; // moved to sub-class
        // int GetAgentId();
        uintptr_t charVtGetAgentId = 0xc4;
        // CharClient::CPlayer* GetPlayer();
        uintptr_t charVtGetPlayer = 0x128;
        // bool IsAlive();
        uintptr_t charVtAlive = 0x4; // moved to sub-class
        // bool IsControlled();
        uintptr_t charVtControlled = 0x174;
        // bool IsDowned();
        uintptr_t charVtDowned = 0x180;
        // bool IsInWater();
        uintptr_t charVtInWater = 0x1b8;
        // bool IsMonster();
        uintptr_t charVtMonster = 0x1c4;
        // bool IsMonsterPlayerClone();
        uintptr_t charVtClone = 0x1c8; // removed in 8/23/2016 patch (assert string not found)
        // bool IsPlayer();
        uintptr_t charVtPlayer = 0x1f4;
        // bool IsRangerPet();
        uintptr_t charVtRangerPet = 0x1f0;
        // Attitude m_attitudeTowardControlled;
        uintptr_t charAttitude = 0x60;
        // CharClient::CBreakBar* m_breakBar;
        uintptr_t charBreakbar = 0x64; // 0x138 = gdBreakbar
        // CharClient::CCoreStats* m_coreStats;
        uintptr_t charCoreStats = 0x1a8;
        // CharClient::CEndurance* m_endurance;
        uintptr_t charEndurance = 0x1cc;
        // CharClient::CHealth* m_health;
        uintptr_t charHealth = 0x1d4;
        // CharClient::CInventory* m_inventory;
        uintptr_t charInventory = 0x1d8;

        uintptr_t charVtGetCmbtnt = 0x84;
        uintptr_t charSkillbar = 0x28c;
        uintptr_t charGliderPercent = 0xb8;
        uintptr_t charProfession = 0x284;
        uintptr_t charName = 0x104;
        uintptr_t charFlags = 0xb0;
        uintptr_t charID = 0x48;
        uintptr_t charSubClass = 0x4; // some offsets have moved to this sub-class
        /*
        Represents a character in the game. Generally stuff that can move around like
        players, npcs or monsters.

        Almost everything in here can be found through assert strings. In order:
        "character->GetAgent() == m_agent"
        "m_agent && (m_agent->GetAgentId() == character->GetAgentId() || m_masterCharacter == character)"
        "m_ownerCharacter->GetPlayer() == CharClientContext()->GetControlledPlayer()"
        Three at once! "character->IsAlive() || (character->IsDowned() && character->IsInWater())"
        "IsControlled()"
        "character->IsAlive() || (character->IsDowned() && character->IsInWater())"
        "character->IsAlive() || (character->IsDowned() && character->IsInWater())"
        Two at once! "IsPlayer() || IsMonster()"
        Two at once! "character->IsPlayer() || character->IsMonsterPlayerClone()"
        "character->IsPlayer() || character->IsMonsterPlayerClone()"
        "m_kennel" then look up for "speciesDef", 3rd vt call counting up

        "m_attitudeTowardControlled < Content::AFFINITY_ATTITUDES"
        "m_breakBar"
        "m_coreStats"
        "!m_endurance"
        "m_health"
        "m_inventory"
        "m_combatant" = char struct shifted +0x18
        "m_skillbar"
        Glider found by looking for float value between 0.0 - 1.0 in your own character data while gliding.
        "m_profession"
        "TextValidateLiteral(m_nameOverride.Ptr())"
        */

        // CharClient::CPlayer
        // char* m_name
        uintptr_t playerName = 0x4c;
        uintptr_t playerVtGetWallet = 0xd0;
        uintptr_t playerVtGetTrainMgr = 0x168;
        uintptr_t playerVtGetAchMgr = 0x98;
        uintptr_t playerVtGetSpecMgr = 0x150;

        uintptr_t playerChar = 0x18;
        /*
        Represents a player.

        The name is very easy to find by just comparing to your name.
        "TextValidateLiteral(m_name.Ptr())"
        "8B C8 8B 10 FF 52 ?? 8B C8 8B 10 FF 92 ?? ?? ?? ?? 8B C8 8B 10 FF 52 ?? 6A 00 50 BA" + 0xD = 32-bit GetWallet fn call
        "m_trainingMgr"
        "achievementMgr"
        "specializationMgr"
        */

        uintptr_t cmbtntBuffBar = 0x50;

        // buff bar struct
        uintptr_t buffbarBuffArr = 0x8;
        uintptr_t buffbarBuffStatArr = 0x44;

        // buff struct
        uintptr_t buffEfType = 0x4;
        uintptr_t buffSkillDef = 0x8;
        uintptr_t buffBuffId = 0xc;
        uintptr_t buffBuffBar = 0x10;
        uintptr_t buffSrcAg = 0x14;
        uintptr_t buffDuration = 0x20;
        uintptr_t buffActive = 0x2c;

        // CharClient::CSkillBar* m_skillBar;
        uintptr_t skillbarSkillsArray = 0x104;
        uintptr_t skillbarPressedSkill = 0x40;
        uintptr_t skillbarHoveredSkill = 0x5c;

        // CharClient::CSkill* m_skillDef
        uintptr_t skillDefEffect = 0x20;
        uintptr_t skillDefInfo = 0x48;

        uintptr_t skillStackType = 0xc;
        uintptr_t skillRadius = 0x44;
        uintptr_t skillRechargeMs = 0x68;
        uintptr_t skillMaxRange = 0x7c;

        uintptr_t walletVtGetCurrency = 0x0;
        uintptr_t trmgrVtGetMLvl = 0xc;
        uintptr_t achMgrVtGetAP = 0xc;
        uintptr_t specMgrSpecsArr = 0x20;
        uintptr_t specMgrTraitsArr = 0x30;
        uintptr_t specType = 0x20;
        uintptr_t traitType = 0x20;

        // CharClient::CCoreStats
        // race
        uintptr_t statsRace = 0x27;
        // gender
        uintptr_t statsGender = 0x29;
        // CharacterStats stats;
        uintptr_t statsStats = 0xa0;
        // int m_level;
        uintptr_t statsLevel = 0x1b8;
        // int m_scaledLevel;
        uintptr_t statsScaledLevel = 0x1e8;
        // Profession m_profession;
        uintptr_t statsProfession = 0x230;
        /*
        Some general stat information about a character.

        Offsets are found by looking at the data. Also see profession enum.
        */

        // CharClient::CEndurance
        // int m_currentValue;
        uintptr_t endCurrent = 0x4;
        // int m_maxValue;
        uintptr_t endMax = 0x8;

        // CharClient::CHealth
        // int m_currentValue;
        uintptr_t healthCurrent = 0x8;
        // int m_maxValue;
        uintptr_t healthMax = 0xc;

        // CharClient::CInventory
        // int m_supply
        uintptr_t invSupply = 0x224;
        uintptr_t invEquipArr = 0xb8;

        uintptr_t equipItemDef = 0x2c;
        uintptr_t equipItemInv = 0x40; // pointer back to char's inventory
        uintptr_t equipItemVtGetSkin = 0x38; // returns item def
        uintptr_t equipItemVtGetUpgrades = 0x104; // returns sigils/runes struct
        uintptr_t equipItemUpgradesArr = 0x18; // call GetUpgrades first
        uintptr_t equipItemUpgradesInfusionsArr = 0xc;

        uintptr_t itemDefId = 0x20;
        uintptr_t itemDefRarity = 0x44;

        // AgentSelection::CContext
        // Agent::CAgentBase* m_autoSelection;
        uintptr_t asctxAuto = 0x28;
        // Agent::CAgentBase* m_hoverSelection;
        uintptr_t asctxHover = 0x84;
        // Agent::CAgentBase* m_lockedSelection;
        uintptr_t asctxLocked = 0x120;
        // D3DXVECTOR3 m_screenToWorld;
        uintptr_t asctxStoW = 0x15c;
        uintptr_t asctxCtxMode = 0x3c; // enum
        uintptr_t asctxVtAgCanSel = 0x4;
        uintptr_t asctxVtSetAct = 0x98;
        /*
        The offsets can be found in a function containing lots of asserts for them. Strings in order:
        "!m_autoSelection"
        "!m_hoverSelection"
        "!m_lockedSelection"
        screenToWorld is easy to find by just moving the cursor around. It's the first vector, because updated more frequently and correct z.
        "m_contextMode == CONTEXT_MODE_NULL"
        "!agent || AgentCanBeSelection(agent)"
        */

        // WorldView::CContext
        // void GetMetrics(int one, D3DXVECTOR3* camPos, D3DXVECTOR3* lookAt, D3DXVECTOR3* upVec float* fov);
        uintptr_t wvctxVtGetMetrics = 0x3c;
        // int m_camStatus;
        uintptr_t wvctxStatus = 0x4c;
        /*
        Context for camera information.

        The GetMetrics function contains the strings "eye", "center", "up", "fov". There are two. Currently
        it is the first one. Set a breakpoint and go up the call stack until you see a virtual call
        that contains the offset.

        camStatus is a bit that flips in loading screens and can be found by inspection.
        */

        uintptr_t camWmAgent = 0x430;
        uintptr_t camCurrZoom = 0x3c;
        uintptr_t camMinZoom = 0x2c;
        uintptr_t camMaxZoom = 0x30;

        // BreakbarState
        uintptr_t breakbarState = 0x20;
        uintptr_t breakbarPercent = 0x24;
        uintptr_t breakbarSkillDef = 0x1c;

        // compass (minimap) offsets
        uintptr_t compWidth = 0x34;
        uintptr_t compHeight = 0x38;
        uintptr_t compZoom = 0x3c;
        uintptr_t compFlags = 0x1c;
        uintptr_t compMaxWidth = 0xc;   // max width?
        uintptr_t compMaxHeight = 0x10; // max height?

        // ui options
        uintptr_t uiIntSize = 0x2c;
        uintptr_t uiFlags1 = 0x158;
        uintptr_t uiFlags2 = 0x15c;

        // gadget stuff
        uintptr_t contextGadget = 0x98;
        uintptr_t ctxgdVtGetGadget = 0x8;
        uintptr_t ctxgdVtGetAtkTgt = 0x10;

        uintptr_t atkTgtGd = 0x30;
        uintptr_t gdHealth = 0x1a4;
        uintptr_t gdWvwTeamId = 0x3f4;
        uintptr_t gdVtGetType = 0x60; // "gadget->GetType() == Content::GADGET_TYPE_RESOURCE_NODE"
        uintptr_t gdVtGetRNode = 0x88; // "Content::GADGET_TYPE_RESOURCE_NODE"

        // resource node stuff
        uintptr_t rnodeType = 0x8;
        uintptr_t rnodeFlags = 0xc;

        // profession stuff
        uintptr_t profStance = 0x20;
        uintptr_t profEnergy = 0x28;
        uintptr_t profEnergyMax = 0x2c;

        // squad stuff
        uintptr_t contextSquad = 0x130;

        uintptr_t contextWorldTime = 0x148;

        uintptr_t contextItem = 0xb0;
        uintptr_t ctxItemVtGetItem = 0x8; // Item* ctx->GetItem(uint itemID);
#endif
    };
};

#endif