#include "main.h"


GW2LIB::Character GW2LIB::GetOwnCharacter()
{
    Character chr;
    chr.m_ptr = GetMain()->GetGameData()->objData.ownCharacter;
    return chr;
}

GW2LIB::Agent GW2LIB::GetOwnAgent()
{
    Agent ag;
    ag.m_ptr = GetMain()->GetGameData()->objData.ownAgent;
    return ag;
}

GW2LIB::Agent GW2LIB::GetAutoSelection()
{
    Agent agent;
    agent.m_ptr = GetMain()->GetGameData()->objData.autoSelection;
    return agent;
}

GW2LIB::Agent GW2LIB::GetHoverSelection()
{
    Agent agent;
    agent.m_ptr = GetMain()->GetGameData()->objData.hoverSelection;
    return agent;
}

GW2LIB::Agent GW2LIB::GetLockedSelection()
{
    Agent agent;
    agent.m_ptr = GetMain()->GetGameData()->objData.lockedSelection;
    return agent;
}

GW2LIB::Vector3 GW2LIB::GetMouseInWorld()
{
    Vector3 pos;
    pos.x = GetMain()->GetGameData()->mouseInWorld.x;
    pos.y = GetMain()->GetGameData()->mouseInWorld.y;
    pos.z = GetMain()->GetGameData()->mouseInWorld.z;
    return pos;
}

int GW2LIB::GetCurrentMapId()
{
    return GetMain()->GetGameData()->mapId;
}

int GW2LIB::GetPing() {
    return GetMain()->GetGameData()->ping;
}

GW2LIB::GW2::UiIntefaceSize GW2LIB::GetUiInterfaceSize() {
    return GetMain()->GetGameData()->uiIntSize;
}

bool GW2LIB::GetUiOptionFlag(GW2LIB::GW2::UiOption opt) {
    if (opt >= GW2LIB::GW2::UI_OPT_END || opt < 0) return false;
    const GameData::GameData *data = GetMain()->GetGameData();

    return !!(opt < 32 ?
        data->uiFlags1 & (1 << opt) :
        data->uiFlags2 & (1 << (opt - 32)));
}

GW2LIB::Compass GW2LIB::GetCompass() {
    Compass comp;
    comp.m_ptr = GetMain()->GetGameData()->objData.compData.get();
    return comp;
}

int GW2LIB::GetFPS() {
    return GetMain()->GetGameData()->fps;
}

Gw2GameHook* get_hook() {
    return &GetMain()->m_gw2Hook;
}

Gw2Hooks* get_hook_list() {
    return &get_hook()->m_hookList;
}

bool GW2LIB::IsInterfaceHidden() {
    return !!GetMain()->GetGameData()->ifHide;
}

bool GW2LIB::IsMapOpen() {
    return !!GetMain()->GetGameData()->mapOpen;
}

bool GW2LIB::IsInCutscene() {
    return GetMain()->GetGameData()->asCtxMode == 1;
}

bool GW2LIB::ActionCamOn() {
    return !!GetMain()->GetGameData()->actionCam;
}

IDirect3DDevice9* GW2LIB::GetD3DDevice() {
    return GetMain()->GetDrawer(false)->getContext();
}

void GW2LIB::AddDrunkLevel(int lvl) {
    auto pmems = GetMain()->GetGameOffsets();
    auto mems  = GetMain()->GetGamePointers();

    hl::ForeignClass ctx = mems->pCtx;
    if (!ctx) return;
    hl::ForeignClass charctx = ctx.get<void*>(pmems->contextChar);
    if (!charctx) return;

    GetMain()->AddDrunkLevel(charctx.data(), lvl);
}

